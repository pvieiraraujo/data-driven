export class FormularioModel {

    constructor(
        public nome?: string,
        public email?: string,
        public cep?: string,
        public numero?: string,
        public complemento?: string,
        public rua?: string,
        public bairro?: string,
        public cidade?: string,
        public estado?: string,

    ) { }

};