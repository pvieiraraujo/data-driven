import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { FormularioModel } from '../template-form/formulario.model';
import { FormValidations } from './../shared/form-validations';
import { Estadobr } from './../shared/modelo/estadobr';
import { ConsultaCepService } from './../shared/services/consulta-cep.service';
import { DropdownService } from './../shared/services/dropdown.service';

@Component({
  selector: 'app-data-form',
  templateUrl: './data-form.component.html',
  styleUrls: ['./data-form.component.css']
})
export class DataFormComponent implements OnInit {

  formulario: FormGroup;
  model = new FormularioModel();
  estados: Estadobr[];
  cargos: any[];
  tecnologias: any[];
  newsletterOp: any[];

  framework = ['Angular', 'React', 'Vue', 'Sencha'];

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    private dropdownService: DropdownService,
    private cepService: ConsultaCepService
  ) {

    this.formulario = this.formBuilder.group({
      nome: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
      email: ['', [Validators.required, Validators.email]],

      cep: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]],
      numero: ['', [Validators.required]],
      complemento: ['', [Validators.maxLength(25)]],
      rua: ['', [Validators.required]],
      bairro: ['', [Validators.required]],
      cidade: ['', [Validators.required]],
      estado: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(2)]],
      cargo: [null],
      tecnologias: [null],
      newsletter: [null],
      termos: [null, Validators.pattern('true')],
      frameworks: this.buildFrameworks()
    });

  }

  buildFrameworks() {

    const values = this.framework.map(v => new FormControl(false));
    return this.formBuilder.array(values, [Validators.required, FormValidations.requiredMinCheckbox(1)]);
    // return [
    //   new FormControl(false),
    //   new FormControl(false),
    //   new FormControl(false),
    //   new FormControl(false)
    // ];
  }


  ngOnInit(): void {

    // this.dropdownService.getEstadosBr()
    // .subscribe(resp => this.estados = resp);

    this.dropdownService.getEstados()
      .subscribe((resp: Estadobr[]) => this.estados = resp);

    this.cargos = this.dropdownService.getCargos();
    this.tecnologias = this.dropdownService.getTecnologias();
    this.newsletterOp = this.dropdownService.getNewsletter();

    // this.formulario = new FormGroup({
    //   nome: new FormControl(null),
    //   email: new FormControl(null),
    // });

  }

  onSubmit() {
    console.log(this.formulario.valid);

    let valueSubmit = Object.assign({}, this.formulario.value);

    valueSubmit = Object.assign(valueSubmit, {
      frameworks: valueSubmit.frameworks
        .map((v, i) => v ? this.framework[i] : null)
        .filter(v => v !== null),
    })

    if (this.formulario) {
      this.http.post('https://httpbin.org/post',
        JSON.stringify(valueSubmit))
        .subscribe(dados => console.log(dados));
      {
        this.formulario.reset();
      }
      (error: any) => alert('erro');
    } else {
      console.log('formulario invalido');
      this.verificaValidacoesForm(this.formulario);
    }
  }


  verificaValidacoesForm(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(campo => {
      console.log(campo);
      const controle = formGroup.get(campo);
      controle.markAsDirty();
      if (controle instanceof FormGroup) {
        this.verificaValidacoesForm(controle);
      }
    })
  }


  resetar() {
    this.formulario.reset();
  }

  verificaValidTouched(campo: string) {
    return !this.formulario.get(campo)?.valid && this.formulario.get(campo)?.touched;
  }

  verificaEmailInvalido() {
    let campoEmail = this.formulario.get('email');
    if (campoEmail.errors) {
      return campoEmail.errors['email'] && campoEmail.touched;
    }
  }


  aplicaCssErro(campo: string) {
    return {
      'has-error': this.verificaValidTouched(campo)
    }
  }

  consultaCEP() {

    const cep = this.formulario.get('cep').value;
    this.getCEP(cep).subscribe(data => {
      this.formulario.get('rua').patchValue(data.logradouro);
      this.formulario.get('complemento').patchValue(data.complemento);
      this.formulario.get('bairro').patchValue(data.bairro);
      this.formulario.get('cidade').patchValue(data.localidade);
      this.formulario.get('estado').patchValue(data.uf);

    });

  }

  getCEP(cep: string): Observable<any> {
    cep = cep?.replace(/\D/g, '');

    if (cep != "") {

      var validacep = /^[0-9]{8}$/;

      if (validacep.test(cep)) {

        return this.http.get(`//viacep.com.br/ws/${cep}/json`);
      }
    }
  }

  setarCargo() {
    const cargo = { nome: 'Dev', nivel: 'Pleno', desc: 'Dev Pl' };
    this.formulario.get('cargo').setValue(cargo);
  }

  compararCargos(obj1, obj2) {
    return obj1 && obj2 ? (obj1.nome === obj2.nome && obj1.nivel === obj2.nivel) : obj1 === obj2;
  }

  setarTecnologias() {
    this.formulario.get('tecnologias').setValue(['java', 'javascript', 'php']);
  }


}
