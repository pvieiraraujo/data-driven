import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CampoControlErroComponent } from './/campo-control-erro/campo-control-erro.component';
import { CardCrudComponent } from './card-crud/card-crud.component';
import { FormDebugComponent } from './form-debug/form-debug.component';
import { DropdownService } from './services/dropdown.service';


@NgModule({
    imports: [
        FormsModule,
        CommonModule,
    ],
    declarations: [
        CardCrudComponent,
        FormDebugComponent,
        CampoControlErroComponent,
    ],
    exports: [
        CardCrudComponent,
        CampoControlErroComponent,
        FormDebugComponent
    ],
    providers: [ DropdownService ],

})
export class SharedModule { }