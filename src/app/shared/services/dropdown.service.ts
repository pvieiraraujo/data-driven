import { Estadobr } from './../modelo/estadobr';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DropdownService {

  constructor(private http: HttpClient) { }


   getEstados() {
    return this.http.get('assets/dados/estadosbr.json');

  }

  // getEstadosBr(){

  // return this.getEstados().pipe(map((resp:Estadobr[])=> resp)); 

  // }

  getCargos(){
    return [
      { nome: 'Dev', nivel: 'Junior', desc: 'Dev Jr'},
      { nome: 'Dev', nivel: 'Pleno', desc: 'Dev Pl'},
      { nome: 'Dev', nivel: 'Senior', desc: 'Dev Sr'}
    ];
  }

  getTecnologias(){
    return [
      {nome:'java', desc:'Java'},
      {nome:'javascript', desc:'JavaScript'},
      {nome:'php', desc:'PHP'},
      {nome:'ruby', desc:'Ruby'},
    ];
  }

  getNewsletter(){
    return [
      { valor: 's', desc: 'Sim' },
      { valor: 'n', desc: 'Não' }
    ]
  }

}
