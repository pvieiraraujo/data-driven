import { FormArray } from '@angular/forms';

export class FormValidations {
    
    static requiredMinCheckbox(min = 1){
        const validator = (formArray : FormArray) => {
          // const values = formArray.controls; 
          // let totalChecked = 0;
          // for (let i = 0; i<ValueTransformer.length; i++){
          //   if (values[i].value) {
          //     totalChecked +=1;
          //   }
          // } forma de validação de checkbox minimo
    
          const totalChecked = formArray.controls
          .map(v => v.value)
          .reduce((total, current) => current ? total + current : total, 0);
          return formArray.pristine || totalChecked >= min ? null : {required: true};
        };
        return validator;
      }

}